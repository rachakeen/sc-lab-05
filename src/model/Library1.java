package model;
import java.util.ArrayList;

public class Library1 {
	private ArrayList<Book1> allBook = new ArrayList<Book1>();
	private ArrayList<SpecialBook1> allSBook = new ArrayList<SpecialBook1>();
	private boolean canLend;
	
	public void addBook(Book1 newBook) {
		allBook.add(newBook);
	}
	
	public void addSpecialBook(SpecialBook1 newSBook) {
		allSBook.add(newSBook);
	}
	
	public void removeBook(Book1 book){
		allBook.remove(book);
	}

	public void removeSpecialBook(SpecialBook1 sBook) {
		allSBook.remove(sBook);
	}
	
	public ArrayList<Book1> getBook() {
		return allBook;
	}
	
	public ArrayList<SpecialBook1> getSpecialBook(){
		return allSBook;
	}
	
	public int getBookCount(){
		return allBook.size();
	}
	
	public int getAllSBookCount(){
		return allSBook.size();
	}
	
	public boolean lendBook(Person1 ps, Book1 bookName) {
		
		if (allBook.contains(bookName) && bookName.getStatus() == "Available"){
			bookName.setStatus("Unavailable");
			removeBook(bookName);
			canLend = true;
		}
		else {
			canLend = false;
		}
		return canLend;
	}
	
	public boolean lendSpecialBook(Person1 ps, SpecialBook1 sBookName) {
		return false;
	}
	
	public void returnBook(Person1 stu, Book1 bookName) {
		bookName.setStatus("Available");
		addBook(bookName);
		canLend = true;
		
	}

}
