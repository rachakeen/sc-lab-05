package model;
public class SpecialBook1 {
	private String bookName;
	private String bookCode;
	private String status;
	
	public SpecialBook1(String bookCode , String bookName) {
		this.bookName = bookName;
		this.bookCode = bookCode;
	}

	public String getBookName() {
		return bookName;
	}

	public void setBookName(String bookName) {
		this.bookName = bookName;
	}

	public String getBookCode() {
		return bookCode;
	}

	public void setYears(String years) {
		this.bookCode = bookCode;
	}
	
	
}