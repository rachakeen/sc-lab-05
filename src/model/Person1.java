package model;
public class Person1 {
	private String personName;
	private String id;
	private String subjectCode;
	
	public Person1(String personName, String id, String subjectCode){
		this.personName = personName;
		this.id = id;
		this.subjectCode = subjectCode;
	}
	
	public String getName(){
		return personName;
	}

	public void setName(String name){
		this.personName = personName;
	}

	public String getId(){
		return id;
	}

	public void setId(String id){
		this.id = id;
	}

	public String getSubjectCode(){
		return subjectCode;
	}
	
	public void setSubjectCode(String subjectCode){
		this.subjectCode = subjectCode;
	}


}
