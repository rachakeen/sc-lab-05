package model;
public class Book1 {
	private String bookCode;
	private String bookName;
	private String status;
	
	public Book1(String bookCode,String bookName){
		this.bookCode = bookCode;
		this.bookName = bookName;
		this.status = "Available";
		
	}
	
	public String getBookName(){
		return bookName;
	}
	public void setBookName(String bookName){
		this.bookName = bookName;
	}
	
	public String getBookCode(){
		return bookCode;
	}
	public void setBookCode(String bookCode){
		this.bookCode = bookCode;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status){
		this.status = status;
	}
}
