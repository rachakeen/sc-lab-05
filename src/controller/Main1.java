package controller;

import model.Book1;
import model.Library1;
import model.Person1;
import model.SpecialBook1;

public class Main1 {
	public static void main(String[] args) {
		Library1 lb = new Library1();
		Person1 ps1 = new Person1("Kantapon", "5610450039", "D14");
		Person1 ps2 = new Person1("Racha", "0123456789", "T14");
		Book1 b1 = new Book1("001", "Math"); 
		Book1 b2 = new Book1("002", "Physic");
		Book1 b3 = new Book1("003", "Algorithm");
		Book1 b4 = new Book1("004", "Java");
		Book1 b5 = new Book1("005", "Calculus");
		Book1 b6 = new Book1("006", "Linear");
		Book1 b7 = new Book1("007", "Biology");
		SpecialBook1 sb1 = new SpecialBook1("101", "NewsPaper");
		SpecialBook1 sb2 = new SpecialBook1("102", "Dictionary");
		
		lb.addBook(b1);
		lb.addBook(b2);
		lb.addBook(b3);
		lb.addBook(b4);
		lb.addBook(b5);
		lb.addBook(b6);
		lb.addBook(b7);
		lb.addSpecialBook(sb1);
		lb.addSpecialBook(sb2);
		
		System.out.println(lb.getBookCount());
		
		System.out.println(lb.lendBook(ps1, b1));
		System.out.println(lb.lendBook(ps2, b2));			
		lb.returnBook(ps1, b1);
		System.out.println(lb.lendBook(ps1, b1));
		System.out.println(lb.lendBook(ps2, b2));
		System.out.println(lb.lendSpecialBook(ps1, sb1));

	}

}
